//
//
// Warnsdorff's Rule for solving Knight's Tour
//
#include <iostream>
#include <iomanip>
#include <array>
#include <string>
#include <tuple>
#include <algorithm>
using namespace std;

template<int N = 8>
class Board
{
public:
	array<pair<int, int>, 8> moves;

	array<array<int, N>, N> data;

	Board()
	{
		// Declares the x & y pair moves that the knight can use
		moves[0] = make_pair(2, 1);
		moves[1] = make_pair(1, 2);
		moves[2] = make_pair(-1, 2);
		moves[3] = make_pair(-2, 1);
		moves[4] = make_pair(-2, -1);
		moves[5] = make_pair(-1, -2);
		moves[6] = make_pair(1, -2);
		moves[7] = make_pair(2, -1);
	}

	array<int, 8> sortMoves(int x, int y) const
	{
		array<tuple<int, int>, 8> counts; // Creates pairs of the 8 different moves
		for (int i = 0; i < 8; ++i)
		{
			int dx = get<0>(moves[i]); // Get the x value of the direction move
			int dy = get<1>(moves[i]); // Get the y value of the direction move

			int c = 0;
			for (int j = 0; j < 8; ++j)
			{
				int x2 = x + dx + get<0>(moves[j]); // Get the x value of the current position + the direction move
				int y2 = y + dy + get<1>(moves[j]); // Get the y value of the current position + the direction move

				if (x2 < 0 || x2 >= N || y2 < 0 || y2 >= N) // If destination falls outside the board try with next move
					continue;
				if (data[y2][x2] != 0) // If destination falls on data field that is already used try with next move
					continue;

				c++;
			}
			counts[i] = make_tuple(c, i); // Make pair of the possible moves that can be performed and the move 
		}
		// Lexicographic sort
		sort(counts.begin(), counts.end());

		array<int, 8> out;
		for (int i = 0; i < 8; ++i)
			out[i] = get<1>(counts[i]);
		return out; // Returns a sorted list of the possible moves and the kind of moves connected to it
	}

	void solve(string start)
	{
		// Clear the board
		for (int v = 0; v < N; ++v)
			for (int u = 0; u < N; ++u)
				data[v][u] = 0;

		// Set start position
		int x0 = start[0] - 'a'; // First char in the string
		int y0 = N - (start[1] - '0'); // Second char in the string
		data[y0][x0] = 1; // Start count with 1

		array<tuple<int, int, int, array<int, 8>>, N*N> order;
		order[0] = make_tuple(x0, y0, 0, sortMoves(x0, y0)); // Start with the first possible moves

		int n = 0;
		while (n < N*N - 1) // Do for every other pieces on the board
		{
			int x = get<0>(order[n]);
			int y = get<1>(order[n]);

			bool ok = false;
			for (int i = get<2>(order[n]); i < 8; ++i) // Check for possible move
			{
				int dx = moves[get<3>(order[n])[i]].first; // First possible move in X direction with best possible outcome
				int dy = moves[get<3>(order[n])[i]].second; // Possible move in Y direction with best possible outcome

				if (x + dx < 0 || x + dx >= N || y + dy < 0 || y + dy >= N) 
					continue; // Skip if any of the destination moves falls out of the board

				if (data[y + dy][x + dx] != 0) 
					continue; // Skip if the destination falls on a field that is already used

				++n;
				get<2>(order[n]) = i + 1; // Set the movement count of the best possible movement 
				data[y + dy][x + dx] = n + 1; // Save data position in data array
				order[n] = make_tuple(x + dx, y + dy, 0, sortMoves(x + dx, y + dy)); // Make the move and go to the next one
				ok = true;
				break;
			}

			if (!ok) // Failed. Backtrack.
			{
				//data[y][x] = 0;
				//4--n;
			}
		}
	}

	template<int N>
	friend ostream& operator<<(ostream &out, const Board<N> &b);
};

template<int N>
ostream& operator<<(ostream &out, const Board<N> &b)
{
	for (int v = 0; v < N; ++v)
	{
		for (int u = 0; u < N; ++u)
		{
			if (u != 0) out << "|";
			out << setw(3) << b.data[v][u];
		}
		out << endl;
	}
	return out;
}

int main()
{
	Board<5> b1; // Define board size
	b1.solve("b4"); // Change start position here
	cout << b1 << endl;

	Board<8> b2; // Board size 8
	b2.solve("b6"); // Solve from position b5
	cout << b2 << endl;

	//Board<31> b3; // Board size 31
	//b3.solve("a1"); // Solve from position a1
	//cout << b3 << endl;
	cin.get();
	return 0;
}